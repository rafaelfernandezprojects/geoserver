# Geoserver #

This javascript file is part of the last project where I was involved. It was created to act as a gateway between the front-end and geoserver (which is a map server app). The whole dependency injection is created to handle requests for defined artic products.

You can signup for a demo by going to https://looknorthservices.com/river-ice-monitoring/