/*global define, window, fetch, Headers, ActiveXObject, DOMParser */


define(['jquery', 'domready!'], function ($) {
    'use strict';

    var maxBounds = {
        getWest: function () {
            return -180;
        },
        getSouth: function () {
            return -90;
        },
        getEast: function () {
            return 180;
        },
        getNorth: function () {
            return 90;
        }
    };

    var timeParams = function (minTime, maxTime) {
        return "tmin:" + minTime.toISOString() + ";tmax:" + maxTime.toISOString() + ";";
    };

    var coordinateParams = function (latlng) {
        return "lon:" + latlng.lng + ";lat:" + latlng.lat + ";";
    };

    var boundParams = function (bb) {
        return "xmin:" + bb.getWest() + ";ymin:" + bb.getSouth() + ";xmax:" + bb.getEast() + ";ymax:" + bb.getNorth() + ";";
    };

    var polyParams = function (latlngs) {
        if (latlngs.length <= 1) {
            return "";
        }

        var latLngCoord = function(latlng) {
            return latlng.lng + " " + latlng.lat;
        };

        var polystring = latlngs.map(latLngCoord).join("\\,") ;// + "\\," + latLngCoord(latlngs[0])
        return "poly:" + polystring + ';';
    };

    var geoServerOutputFormat = function(format) {
        var formats = {
            csv: 'csv',
            json: 'application/json',
            shp: 'shape-zip',
            kml: 'kml',
        }
        return formats[format];
    };

    return function (workspace, onError) {
        var authHeaders = new Headers({
            'Content-type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('userJWT')
        });

        var wmsUrl = function () {
            return "/geoserver/" + workspace + "/wms";
        };

        var wfsRequest = function (layerName, options, wantResponseObj) {
            options = options || {};
            if (!options.outputFormat) {
                options.outputFormat = 'application/json';
            }else{
                options.outputFormat = geoServerOutputFormat(options.outputFormat);
            }
            var url = "/geoserver/" + workspace + "/ows?" + $.param(Object.assign({}, {
                version: '1.0.0',
                maxFeatures: 2900
            }, options, {
                service: 'WFS',
                request: 'GetFeature',
                typeName: workspace + ":" + layerName
            }));

            return fetch(url, {
                method: 'GET',
                headers: authHeaders
            }).then(function (response) {
                if (response.ok && response.status === 200) {
                    if (wantResponseObj) {
                        return response;
                    }else{
                        var contentType = response.headers.get('content-type');
                        if(contentType && contentType.indexOf('application/json') !== -1) {
                            return response.json();
                        }
                        return response.text();
                    }
                } else {
                    return Promise.reject(response);
                }
            }).catch(onError);
        };

        var getCapabilitiesRequest = function () {
            return fetch('/geoserver/' + workspace + '/wms?service=wfs&version=1.0.0&request=GetCapabilities', {
                method: 'GET',
                headers: authHeaders
            }).then(function (response) {
                if (response.ok && response.status === 200) {
                    return response.text();
                } else {
                    return Promise.reject(response);
                }
            }).then(function (xmlTxt) {
                if (window.DOMParser) {
                    var parser = new DOMParser();
                    return parser.parseFromString(xmlTxt, "text/xml");
                } else { // Internet Explorer
                    var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = false;
                    xmlDoc.loadXML(xmlTxt);
                    return xmlDoc;
                }
            }).catch(onError);
        };

        var wmsLegendUrl = function (layerName, params) {
            params = params || {};
            return wmsUrl() + "?" + $.param(Object.assign({}, {
                version: '1.0.0',
                format: 'image/png',
                width: 20,
                height: 20,
                transparent: true,
                legend_options: 'forceLabels:on'
            }, params, {
                request: 'GetLegendGraphic',
                layer: workspace + ":" + layerName,
                token: localStorage.getItem('userJWT')
            }));
        };

        var wmsDefaultParams = function (layerName, params) {
            params = params || {};
            return Object.assign({}, {
                token: localStorage.getItem('userJWT'),
                layers: workspace + ":" + layerName,
                format: 'image/png8',
                transparent: true,
                version: '1.1.1',
                tiled: true
                
            }, params);
        };

        var timeAndPositionLayerUrl = function (layerName, timestamp, position, options) {
            options = options || {};
            return wfsRequest(layerName, Object.assign({}, options, {viewparams: "timestamp:" + timestamp.toISOString() + ";" + coordinateParams(position)}));
        };

        var timeAndBoundsLayerUrl = function (layerName, timestamp, bounds, options) {
            options = options || {};
            return wfsRequest(layerName, Object.assign({}, options, {viewparams: "timestamp:" + timestamp.toISOString() + ";" + boundParams(bounds)}));
        };

        var timeRangeAndPositionLayerUrl = function (layerName, minTime, maxTime, position, options) {
            options = options || {};
            return wfsRequest(layerName, Object.assign({}, options, {viewparams: timeParams(minTime, maxTime) + coordinateParams(position)}));
        };

        var timeWithTimeRangeAndPositionLayerUrl = function (layerName, timestamp, minTime, maxTime, position, options) {
            options = options || {};
            return wfsRequest(layerName, Object.assign({}, options, {viewparams: "timestamp:" + timestamp.toISOString() + ";" + timeParams(minTime, maxTime) + coordinateParams(position)}));
        };

        var timeRangeAndBoundLayerUrl = function (layerName, minTime, maxTime, testBounds, options, wantResponseObj) {
            options = options || {};
            testBounds = testBounds || maxBounds;
            return wfsRequest(layerName, Object.assign({}, options, {viewparams: timeParams(minTime, maxTime) + boundParams(testBounds)}), wantResponseObj);
        };

        var imageProductType = function (queryImageLayer, queryGeometryLayer, queryDistinctTimestampLayer) {
            return {
                queryImages: function (productId) {
                    return wfsRequest(queryImageLayer, {cql_filter: "(PRODUCT_ID IN ('" + productId + "'))"});
                },
                queryGeometry: function (minTime, maxTime, bounds, options) {
                    options = options || {};
                    var timeFilter = function (attr, jsDateTimeMin, jsDateTimeMax) {
                        return "(" + attr + " > '" + jsDateTimeMin.toISOString() + "') AND (" + attr + " < '" + jsDateTimeMax.toISOString() + "')";
                    };
                    var boundsStr = bounds.getWest() + "," + bounds.getSouth() + "," + bounds.getEast() + "," + bounds.getNorth();
                    return wfsRequest(queryGeometryLayer, Object.assign({}, options, {cql_filter: timeFilter("TIME", minTime, maxTime) + " AND BBOX(LOCATION," + boundsStr + ")"}));
                },
                queryDistinctTimestamp: wfsRequest.bind(null,queryDistinctTimestampLayer)
            };
        };

        var ddsProductType = function (queryGeometryLayer) {
            return {
                queryGeometry: function (bounds, options) {
                    options = options || {};
                    var boundsStr = bounds.getWest() + "," + bounds.getSouth() + "," + bounds.getEast() + "," + bounds.getNorth();
                    return wfsRequest(queryGeometryLayer, Object.assign({}, options, {cql_filter: " BBOX(target," + boundsStr + ")"}));
                }
            };
        };

        var imageRasterProductType = function (geometryLayerName, imageLayerName, timeRangeLayerName, specificImageLayerName) {
            return {
                queryGeometry: timeRangeAndPositionLayerUrl.bind(null, geometryLayerName),
                queryImages: timeRangeAndBoundLayerUrl.bind(null, imageLayerName),
                queryTimeRange: wfsRequest.bind(null, timeRangeLayerName),
                querySpecificImages: timeAndPositionLayerUrl.bind(null, specificImageLayerName)
            };
        };

        var wmsRasterType = function (layerName) {
            return {
                legendUrl: wmsLegendUrl.bind(null, layerName),
                defaultParams: wmsDefaultParams.bind(null, layerName)
            };
        };

        return {
            getCapabilities: getCapabilitiesRequest,
            pipeline: Object.assign({},
                    wmsRasterType("InfrastructurePipelineWMS"),
                    {wmsUrl: wmsUrl, 
                queryGeometry: wfsRequest.bind(null, "InfrastructurePipelineWMS")
            }),
            watertemp: Object.assign({},
                    //wmsRasterType("QueryWaterTemperatureVecRaster"),
                    wmsRasterType("WaterTemperatureWMS"),
                    {
                wmsUrl: wmsUrl,
                queryGeometry: function (minTime, maxTime, latlngs, options) {
                    options = options || {};
                    //return timeRangeAndBoundLayerUrl("QueryWaterTemperatureSummary_min_max_date_bbox", minTime, maxTime, latlngs, options);
                    return timeRangeAndBoundLayerUrl("WaterTemperatureAreaBounds", minTime, maxTime, latlngs, options);
                },
                queryPointChartData: timeRangeAndPositionLayerUrl.bind(null, "WaterTemperaturePointChartData"),
                queryAverageWithinBounds: timeAndBoundsLayerUrl.bind(null, "WaterTemperatureWeightedByArea")
            }),
            watersediment: Object.assign({},
                    //wmsRasterType("QueryWaterSedimentVecRaster"),
                    wmsRasterType("WaterSedimentWMS"),
                    {
                wmsUrl: wmsUrl
                /*
                queryGeometry: function (minTime, maxTime, latlngs, options) {
                    options = options || {};
                    //return timeRangeAndBoundLayerUrl("QueryWaterSedimentSummary_min_max_date_bbox", minTime, maxTime, latlngs, options);
                    return timeRangeAndBoundLayerUrl("WaterSedimentAreaBounds", minTime, maxTime, latlngs, options);
                },
                queryPointChartData: timeRangeAndPositionLayerUrl.bind(null, "WaterSedimentPointChartData"),
                queryObservationCounts: wfsRequest.bind(null, "WaterSedimentObservationCounts")
                */
            }),
            riverice: Object.assign({},
                    wmsRasterType("RiverIce"),
                    {
                wmsUrl: wmsUrl,
                queryGeometry: function (minTime, maxTime, latlngs, options) {
                    options = options || {};
                    return timeRangeAndBoundLayerUrl("RiverIceAreaBounds", minTime, maxTime, latlngs, options);
                },
                queryPointChartData: timeRangeAndPositionLayerUrl.bind(null, "RiverIcePoint"),
//                queryObservationCounts: wfsRequest.bind(null, "QueryRiverIce2ObservationCounts"),
                queryChartData: timeRangeAndBoundLayerUrl.bind(null, "RiverIceArea"),
                queryChartDataStarted: timeRangeAndBoundLayerUrl.bind(null, "RiverIceTimeMV")
            }),
            smartice: Object.assign({}, 
                wmsRasterType("SmartIceMaps"),
            {
                wmsUrl: wmsUrl,
                queryGeometry: function (minTime, maxTime, latlngs, options) {
                    options = options || {};
                    return timeRangeAndBoundLayerUrl("SmartIceMapsAreaBounds", minTime, maxTime, latlngs, options);
                },
                queryPointChartData: timeRangeAndPositionLayerUrl.bind(null, "SmartIceMapsPoint"),
                queryPointChartData_q: function (minTime, maxTime, latlngs, search_area, options) {
                    options = options || {};
                    return wfsRequest("SmartIceQamutikPoint", Object.assign({}, options, {viewparams: timeParams(minTime, maxTime) + coordinateParams(latlngs) + "search_area:" + search_area + ";"}));
                    //return timeRangeAndPositionLayerUrl("SmartIceQamutikPoint", minTime, maxTime, latlngs, options);
                },
                //queryPointChartData_q: timeRangeAndPositionLayerUrl.bind(null, "SmartIceQamutikPoint",  "rad:" + '10' + ";"),
                queryEvent_q: timeRangeAndPositionLayerUrl.bind(null, "SmartIceQamutikEvent"),

                queryGeometry_q: function (minTime, maxTime, latlngs, options) {
                    options = options || {};
                    return timeRangeAndBoundLayerUrl("SmartIceQamutikAreaBounds", minTime, maxTime, latlngs, options);
                },
                product_q: wmsDefaultParams.bind(null, "SmartIceQamutik"),
                legendUrl_q: wmsLegendUrl.bind(null, "SmartIceQamutik")

                //queryProductGeometry_q: wfsRequest.bind(null, "SmartIceQamutikAreaBounds")
                //queryChartData: timeRangeAndBoundLayerUrl.bind(null, "SmartIceMapsArea")

                //legendUrl: wmsLegendUrl.bind(null, "QuerySmartIceVecRaster"),
                //product: wmsDefaultParams.bind(null, "QuerySmartIceVecRaster"),
                //sentinelOneRaster: wmsDefaultParams.bind(null, "SmartIceRaster2"),
                //wmsUrl: wmsUrl,
                //queryProductGeometry: wfsRequest.bind(null, "QuerySmartIceSummary"),
                //querySentinelOneGeometry: wfsRequest.bind(null, "QuerySmartIceRaster2Summary"),
                //queryPointChartData: timeRangeAndPositionLayerUrl.bind(null, "QuerySmartIcePointChartData"),
                //queryObservationCounts: wfsRequest.bind(null, "QuerySmartIceObservationCounts")
            }),
            greenness: Object.assign({},
                    wmsRasterType("QueryGreennessRaster"),
                    {
                wmsUrl: wmsUrl,
                queryDistinctTimestamp: wfsRequest.bind(null,"QueryGreennessDistinctTimestamps"),
                queryAreaChartData: timeRangeAndBoundLayerUrl.bind(null, "QueryGreennessAreaChartData"),
                queryPointChartData: timeRangeAndPositionLayerUrl.bind(null, "QueryGreennessPointChartData")
            }),
            icebergs: Object.assign({},
                    wmsRasterType("QueryIcebergsRaster"),
                    {wmsUrl: wmsUrl, //queryTimeRange: wfsRequest.bind(null, "QueryInfrastructureCountingTimeRange")}),
                    queryDistinctTimestamp: wfsRequest.bind(null,"QueryInfrastructureDistinctTimestamps",{viewparams: "types:'Iceberg';"})}),
            trailers: Object.assign({},
                    wmsRasterType("QueryTrailersRaster"),
                    {wmsUrl: wmsUrl, //queryTimeRange: wfsRequest.bind(null, "QueryInfrastructureCountingTimeRange")}),
                    queryDistinctTimestamp: wfsRequest.bind(null,"QueryInfrastructureDistinctTimestamps",{viewparams: "types:'Trailer';"})}),
            planes: Object.assign({},
                    wmsRasterType("QueryPlanesRaster"),
                    {wmsUrl: wmsUrl, //queryTimeRange: wfsRequest.bind(null, "QueryInfrastructureCountingTimeRange")}),
                    queryDistinctTimestamp: wfsRequest.bind(null,"QueryInfrastructureDistinctTimestamps",{viewparams: "types:'LargePlane'\\,'SmallPlane';"})}),
            infrastructure: Object.assign({},
                    //wmsRasterType("QueryInfrastructureCountingRaster"),
                    wmsRasterType("InfrastructureWMS"),
                    {
                wmsUrl: wmsUrl,
                queryGeometry: timeRangeAndBoundLayerUrl.bind(null, "InfrastructureData"),
                //queryTimeRange: wfsRequest.bind(null, "QueryInfrastructureCountingTimeRange"),
                queryDistinctTimestamp: wfsRequest.bind(null,"InfrastructureDates",{viewparams: "types:'Residential'\\,'Commercial'\\,'Garage'\\,'FARM'\\,'Other';"}),
                queryCountsWithinRadius: function (minTime, maxTime, position, radius, options) {
                    options = options || {};
                    return wfsRequest("InfrastructureDistance", Object.assign({}, options, {viewparams: timeParams(minTime, maxTime) + coordinateParams(position) + "rad:" + radius + ";"}));
                },
                /*queryCountsWithinPolygon: function (minTime, maxTime, latlngs, options) {
                    options = options || {};
                    return wfsRequest("QueryInfrastructureWithinPolygon", Object.assign({}, options, {viewparams: timeParams(minTime, maxTime) + polyParams(latlngs)}))
                },*/
                queryCountsWithinLineBuffer: function (minTime, maxTime, latlngs, buffer, options) {
                    options = options || {};
                    return wfsRequest("InfrastructureBuffer", Object.assign({}, options, {viewparams: timeParams(minTime, maxTime) + polyParams(latlngs) + "buffer:" + buffer + ";"}))
                },
                getRasterImage: function(layerName)
                {
                    return { defaultParams:wmsDefaultParams.bind(null, layerName)};
                }
            }),
            stockpiling: Object.assign({},
                    wmsRasterType("QueryStockPilingRaster"),
                    {wmsUrl: wmsUrl, //queryTimeRange: wfsRequest.bind(null, "QueryInfrastructureCountingTimeRange")}),
                    queryDistinctTimestamp: wfsRequest.bind(null,"QueryInfrastructureDistinctTimestamps",{viewparams: "types:'Material'\\,'Equipment';"})}),
            vegetation: Object.assign({},
                    wmsRasterType("QueryVegetationRaster"),
                    {
                wmsUrl: wmsUrl,
                queryDistinctTimestamp: wfsRequest.bind(null,"QueryVegetationDistinctTimestamps"),
                //queryTimeRange: wfsRequest.bind(null, "QueryVegetationTimeRange"),
                queryChartData: timeRangeAndBoundLayerUrl.bind(null, "QueryVegetationData")
            }),
            encroachment: imageProductType("QueryEncroachmentImages", "QueryEncroachmentGeometry","QueryEncroachmentDistinctTimestamps"),
            dds: ddsProductType("QueryPThreatGeometry"),
            waterbody: imageProductType("QueryWaterBodyImages", "QueryWaterBodyGeometry","QueryWaterBodyDistinctTimestamps"),
            landDeformation: Object.assign({},
                    imageRasterProductType("QueryLandDeformation", "QueryLandDeformationRasterSingle", "QueryLandDeformationTimeRange", "QueryLandDeformationRasterSpecific"),
                    {
                queryDistinctTimestamp: wfsRequest.bind(null,"QueryLandDeformationDistinctTimestamps"),
                legendUrl: function () {
                    return './data/raster/legend-land-deformation.jpg';
                }
            }),
            waterQuality: Object.assign({},
                    imageRasterProductType("QueryWaterQualityChloro", "QueryWaterQualityRasterSingle", "QueryWaterQualityTimeRange", "QueryWaterQualityRasterSpecific"),
                    wmsRasterType("QueryWaterQualityRaster"),
                    {
                wmsUrl: wmsUrl,
                queryDistinctTimestamp: wfsRequest.bind(null,"QueryWaterQualityDistinctTimestamps"),
                queryAverageGeometry: function (minTime, maxTime, position, options) {
                    options = options || {};
                    return wfsRequest("QueryWaterQualityChloroAverage", Object.assign({}, options, {viewparams: timeParams(minTime, maxTime) + coordinateParams(position) + 'datetrunc:month;'}));
                },
                legendUrl: function () {
                    return './data/raster/legend2.jpg';
                }
            })
        };
    };
});
